
#include "Pathfinder.h"

#include <cassert>
#include <iostream>
#include <vector>
#include <chrono>

// The map structure combines all the data we need to represent a map
struct Map
{
	int startX, startY;
	int targetX, targetY;
	std::vector<unsigned char> map;
	int width, height;
};

static const Map map1 = {
	0, 0, 1, 2,
	{1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1},
	4, 3
};

static const Map map2 = {
	2, 0, 0, 2,
	{0, 0, 1, 0, 1, 1, 1, 0, 1},
	3, 3
};

static const Map map3 = {
	4, 2, 5, 4,
	{1, 1, 1, 1, 1, 1, 1, 1, 
	 1, 0, 1, 1, 1, 1, 0, 1, 
	 1, 0, 1, 1, 1, 1, 0, 1, 
	 1, 1, 0, 0, 0, 0, 1, 1, 
	 1, 1, 1, 1, 1, 1, 1, 1},
	8, 5
};

static const Map map4 = {
	0, 0, 0, 7,
	{1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1,
	 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1,
	 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1,
	 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
	 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1,
	 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1},
	16, 8
};

static const Map map5 = {
	1, 5, 7, 1,
	{1, 1, 1, 1, 1, 1, 1, 1,
	 1, 0, 0, 0, 0, 0, 0, 1,
	 1, 1, 1, 1, 1, 1, 0, 1,
	 1, 1, 1, 1, 1, 1, 0, 1,
	 1, 1, 1, 1, 1, 1, 0, 1,
	 1, 1, 1, 1, 1, 1, 0, 1,
	 1, 0, 0, 0, 0, 0, 0, 1,
	 1, 1, 1, 1, 1, 1, 1, 1},
	8, 8
};

static int FindPath(const Map& map, int* pOutBuffer, const int nOutBufferSize)
{
	return FindPath(map.startX, map.startY,
		            map.targetX, map.targetY,
		            map.map.data(), map.width, map.height,
		            pOutBuffer, nOutBufferSize);
}

static int ProfileFindPath(const Map& map, const int numIterations, int* pOutBuffer, const int nOutBufferSize)
{
	namespace chr = std::chrono;

	volatile int ret = 0;

	const auto start = chr::high_resolution_clock::now();
	for (int i = 0; i < numIterations; i++)
	{
		ret += FindPath( map, pOutBuffer, nOutBufferSize );
	}
	const auto end = chr::high_resolution_clock::now();

	const chr::duration<double, std::milli> time = (end - start) / numIterations;
	std::cout << time.count() << " ms\n";

	return ret;
}

static void Test1()
{
	int pOutBuffer[12];
	int result = FindPath(map1, pOutBuffer, 12);
	assert(result == 3);
	assert(pOutBuffer[0] == 1);
	assert(pOutBuffer[1] == 5);
	assert(pOutBuffer[2] == 9);
}

static void Test2()
{
	int pOutBuffer[7];
	int result = FindPath(map2, pOutBuffer, 7);
	assert(result == -1);
}

static void Test()
{
	std::cout << "Running tests...\n";
	Test1();
	Test2();
	std::cout << "All tests finished successfully.\n";
}

// Draw a map using ascii art
static void DrawMap(const Map& map, const int* pPathBuffer = nullptr, const int nPathBufferSize = 0)
{
	for (int y = 0; y < map.height; y++)
	{
		std::cout << '|';
		for (int x = 0; x < map.width; x++)
		{
			char c = ' ';

			// Check if square is start
			if (x == map.startX && y == map.startY)
				c = '^';
			
			// Check if square is target
			else if (x == map.targetX && y == map.targetY)
				c = '$';
			
			// Check if square is unpassable
			else if (map.map[y * map.width + x] == 0)
				c = '#';

			// Check if square is in path (excluding last one)
			for (int i = 0; i < nPathBufferSize - 1; i++)
			{
				int px = pPathBuffer[i] % map.width;
				int py = pPathBuffer[i] / map.width;
				if (x == px && y == py)
					c = '.';
			}

			std::cout << c;
		}
		std::cout << "|\n";
	}
	std::cout << '\n';
}

// Helper function to both solve and draw a map
static void DrawMapWithPath(const Map& map)
{
	int pOutBuffer[64];
	int result = FindPath(map, pOutBuffer, std::size(pOutBuffer));
	DrawMap(map, pOutBuffer, result);
}

static int ProfilePathfinder()
{
	const int numIterations = 100000;
	const Map maps[] = {map1, map2, map3, map4, map5};
	
	volatile int dontOptimizeAway = 0;
	int pOutBuffer[64];

	for (int i = 0; i < std::size(maps); i++)
	{
		std::cout << "Profiling map " << i << " : ";
		dontOptimizeAway += ProfileFindPath( maps[i], numIterations, pOutBuffer, std::size( pOutBuffer ) );
	}

	return dontOptimizeAway;
}

static int StressTest()
{
	const int numIterations = 100;

	Map stressTestMap;
	{
		const int mapSize = 2048;
		stressTestMap.width = mapSize;
		stressTestMap.height = mapSize;
		stressTestMap.map.resize(mapSize * mapSize, 1);
		stressTestMap.targetX = 0;
		stressTestMap.targetY = 0;
		stressTestMap.startX = mapSize - 1;
		stressTestMap.startY = mapSize - 1;
	}
	
	volatile int dontOptimizeAway = 0;
	int pOutBuffer[64];

	std::cout << "Profiling stress test map : ";
	dontOptimizeAway += ProfileFindPath(stressTestMap, numIterations, pOutBuffer, std::size(pOutBuffer));

	return dontOptimizeAway;
}

static void DrawSampleMaps()
{
	const Map maps[] = {map1, map2, map3, map4, map5};

	DrawMapWithPath(map1);
	DrawMapWithPath(map2);
	DrawMapWithPath(map3);
	DrawMapWithPath(map4);
	DrawMapWithPath(map5);
}

int main()
{
	int ret = 0;

	enum Task { DRAW_SAMPLES, TEST, PROFILE, STRESS_TEST, QUIT, INVALID };
	const char* taskNames[] = { "Run sample maps", "Run tests", "Run profile", "Run stress test", "Quit" };

	std::cout << "Welcome to Pathfinder demo. ";

	while (true)
	{
		// Display options and get user input
		std::cout << "Please select an option.\n";
		for (int i = 0; i < std::size(taskNames); i++)
		{
			std::cout << " [" << i << "]  " << taskNames[i] << std::endl;
		}

		Task task = INVALID;
		while (task < 0 || task >= INVALID )
		{
			task = static_cast<Task>(getchar() - '0');
		}

		switch (task)
		{
			case DRAW_SAMPLES:
				DrawSampleMaps();
				break;

			case TEST:
				Test();
				break;

			case PROFILE:
				ret = ProfilePathfinder();
				break;

			case STRESS_TEST:
				ret = StressTest();
				break;

			case QUIT:
				return 0;
		}

		std::cout << '\n';
	}

	return ret;
}
