
#include "Pathfinder.h"

#include <cassert>
#include <queue>
#include <unordered_map>
#include <vector>

struct Point { int x; int y; };

// Get a map square's coordinates via its index
Point GetPoint(int index, int nMapWidth)
{
	Point p;
	p.x = index % nMapWidth;
	p.y = index / nMapWidth;
	return p;
}

// Get the index of a map square via its coordinates
int GetIndex(Point p, int nMapWidth)
{
	return p.x + p.y * nMapWidth;
}

// Returns true if a given square is on the map and is not an obstacle
bool IsWalkable(const Point& p, const unsigned char* pMap, const int nMapWidth, const int nMapHeight)
{
	return p.x >= 0 && p.x < nMapWidth
		&& p.y >= 0 && p.y < nMapHeight
		&& pMap[GetIndex(p, nMapWidth)] != 0;
}

// Helper function to build a path once the target has been found
// getPreviousCallback is a function to get the index of a square's previous square in the path.
template <typename F>
static int BuildPath(const int nStartIndex, const int nTargetIndex, const F& getPreviousCallback,
                     int length, int* pOutBuffer, const int nOutBufferSize)
{
	// Start at the end and work back
	int square = nTargetIndex;
	for (int i = length - 1; i >= 0; i--)
	{
		assert(square != nStartIndex);
		if (i < nOutBufferSize)
			pOutBuffer[i] = square;
		square = getPreviousCallback(square);
	}

	return length;
}

static int Heuristic(const Point a, const Point b)
{
	// A* heuristic using Manhattan distance
	return abs(a.x - b.x) + abs(a.y - b.y);
}

int FindPath(const int nStartX, const int nStartY,
             const int nTargetX, const int nTargetY, 
             const unsigned char* pMap, const int nMapWidth, const int nMapHeight,
             int* pOutBuffer, const int nOutBufferSize)
{
	// The frontier is a priority queue of squares to be examined
	using FrontierSquare = std::pair<int, int>;
	using FrontierVector = std::vector<FrontierSquare>;
	using FrontierQueue = std::priority_queue<FrontierSquare, FrontierVector, std::greater<>>;

	FrontierQueue frontier;
	const int startSquare = GetIndex({nStartX, nStartY}, nMapWidth);
	frontier.push({0, startSquare});

	// We need a map of all visited squares, with the index of the previously visited square,
	// and the accumulated cost of getting to that square from the start point.
	struct SquareInfo
	{
		int previous;
		int accumulatedCost;
	};
	
	// std::unordered_map found to be ~25% faster than std::map
	std::unordered_map<int, SquareInfo> visitedSquares;
	visitedSquares[startSquare] = {-1, 0};

	// Loop until the target is found, or until there are no more squares to explore
	while (!frontier.empty())
	{
		int square = frontier.top().second;
		frontier.pop();

		const SquareInfo info = visitedSquares.find(square)->second;

		int currentCost = info.accumulatedCost;
		
		Point p = GetPoint(square, nMapWidth);
		
		if (p.x == nTargetX && p.y == nTargetY)
		{
			// Found the target, now build path and return it
			auto getPrevious = [&](int i){ return visitedSquares.find(i)->second.previous; };
			return BuildPath(startSquare, square, getPrevious, currentCost, pOutBuffer, nOutBufferSize);
		}

		// Check all the current square's neighbours
		Point neighbors[4] = {
			{ p.x,     p.y - 1 }, // North
			{ p.x - 1, p.y     }, // West
			{ p.x,     p.y + 1 }, // South
			{ p.x + 1, p.y     }, // East
		};

		for (Point n : neighbors)
		{
			if (!IsWalkable(n, pMap, nMapWidth, nMapHeight))
				continue;

			// Square is walkable, check if it's been visited
			int neighborIndex = GetIndex(n, nMapWidth);
			int cost = currentCost + 1;

			if (visitedSquares.find(neighborIndex) == visitedSquares.end())
			{
				// Square hasn't been visited, add it to the frontier and set
				// the priority to the combined heuristic and cumulative cost.
				int priority = cost + Heuristic(n, Point{nTargetX, nTargetY});
				frontier.push({priority, neighborIndex});
				visitedSquares[neighborIndex] = {square, cost};
			}
		}
	}

	// No path found
	return -1;
}
